package net.nyacom.davy.lightsout;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class LightsActivity extends Activity implements LightsView.LightsViewEventListener {
	private LightsMatrix mat;
	private ValueAnimator anim;
	private LightsView gameView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lights);
		gameView=(LightsView)findViewById(R.id.lightsView);
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			setupValueAnimator();
		}
		LightsMatrix savedMat=null;
		if(savedInstanceState!=null) {
			savedMat=(LightsMatrix) savedInstanceState.getSerializable(SAVESTATE_MATKEY);
		}
		handleNewGame(savedMat);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupValueAnimator() {
		anim=ValueAnimator.ofInt(255,0);
		anim.setDuration(400);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void addAnimatorListener() {
		anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				gameView.setPrevAlpha((Integer)animation.getAnimatedValue());
				gameView.invalidate();
			}
		});
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void startAnimation() {
		anim.start();
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void removeAnimatorListener() {
		anim.removeAllUpdateListeners();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lights, menu);
		return true;
	}
	@Override
	public void onResume() {
		super.onResume();
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			addAnimatorListener();
		}
	}
	@Override
	public void onPause() {
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			removeAnimatorListener();
		}
		super.onPause();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent=new Intent(this,SettingsActivity.class);
			startActivity(intent);
		} else if(id==R.id.action_newgame) {
			handleNewGame();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	public void cellClicked(LightsView view, int tilex, int tiley) {
		if(mat.hasBeenCleared()) {
			handleNewGame();
		} else {
			mat.flipXY(tilex, tiley);
			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
				startAnimation();
			} else {
				findViewById(R.id.lightsView).invalidate();
			}
			if(mat.isCleared()) {
				handleClear();
			}
		}
	}
	private void handleClear() {
		Toast.makeText(this, "Cleared", Toast.LENGTH_SHORT).show();
	}
	private void handleNewGame() { handleNewGame(null); }
	private void handleNewGame(LightsMatrix savedMat) {
		gameView.setLightsViewEventListener(this);
		SharedPreferences shrP=PreferenceManager.getDefaultSharedPreferences(this);
		if(savedMat==null) {
			mat=new LightsMatrix(shrP.getInt(SettingsActivity.PREF_WIDTH, SettingsActivity.DEF_WIDTH),
					shrP.getInt(SettingsActivity.PREF_HEIGHT, SettingsActivity.DEF_HEIGHT));
			mat.randomize(shrP.getInt(SettingsActivity.PREF_NLOOPS, SettingsActivity.DEF_LOOPS));
		} else {
			mat=savedMat;
		}
		gameView.setMatrix(mat);
		if(savedMat==null) {
			gameView.optimizeViewPort();
		}
		gameView.invalidate();
	}
	private static final String SAVESTATE_MATKEY="LightsOut_Mat";
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(SAVESTATE_MATKEY, mat);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mat=(LightsMatrix) savedInstanceState.getSerializable(SAVESTATE_MATKEY);
		gameView.setMatrix(mat);
	}
}
