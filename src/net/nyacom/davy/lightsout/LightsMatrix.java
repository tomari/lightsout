package net.nyacom.davy.lightsout;

import java.io.Serializable;
import java.util.Random;

public class LightsMatrix implements Serializable {
	private static final long serialVersionUID = 1576863936409678027L;
	private long[] mat;
	private long[] prevmat;
	private int width;
	public static int MAT_FRONT=0;
	public static int MAT_BACK=1;
	private boolean clearedCache=false;
	public LightsMatrix(int width, int height) {
		mat=new long[height];
		prevmat=new long[height];
		this.width=width;
	}
	public int getWidth() { return width; }
	public int getHeight() { return mat.length; }
	private long bitn(int n) { return (1l<<n); }
	private int lastX, lastY;
	public boolean getXY(int mat, int x, int y) {
		long[] m=mat==MAT_FRONT?this.mat:this.prevmat;
		return (bitn(x)&m[y])>0;
	}
	public void flipXY(int x, int y) {
		for(int i=0; i<mat.length; i++) {
			prevmat[i]=mat[i];
		}
		long colmask=bitn(x);
		try { mat[y-1]=(mat[y-1]^colmask); } catch (IndexOutOfBoundsException e) { }
		try { mat[y+1]=(mat[y+1]^colmask); } catch (IndexOutOfBoundsException e) { }
		long colmask3=(0x3l<<x);
		if(x>0) {
			colmask3|=bitn(x-1);
		}
		mat[y]=(mat[y]^colmask3);
		lastX=x;
		lastY=y;
	}
	public boolean isCleared() {
		if(!clearedCache) {
			long mask=bitn(width)-1;
			for(long row: mat) {
				if((row&mask)>0) {
					return false;
				}
			}
			clearedCache=true;
		}
		return true;
	}
	public boolean hasBeenCleared() {
		return clearedCache;
	}
	public void randomize(int n) {
		Random r=new Random();
		while(n-->0) {
			int x=r.nextInt(getWidth());
			int y=r.nextInt(getHeight());
			flipXY(x,y);
		}
	}
	public int getLastX() { return lastX; }
	public int getLastY() { return lastY; }
}
