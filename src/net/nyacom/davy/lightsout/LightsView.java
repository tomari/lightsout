package net.nyacom.davy.lightsout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class LightsView extends View {
	private LightsMatrix mat;
	private int clipXleft, clipXright, clipYtop, clipYbottom;
	private float zoom, initialZoom;
	private static final float ZOOM_MIN=.1f, ZOOM_MAX=10.f;
	private int offsetX, offsetY;
	private int screenCellSize=1;
	private static final float CELLSIZE=32.f;
	private Paint filledPaint, emptyPaint;
	private Path drawPathF, drawPathE;
	private RectF rect=new RectF();
	private int prevAlpha=0;
	private GestureDetector gestureDetector;
	private ScaleGestureDetector scaleGestureDetector;
	public LightsView(Context context) {
		super(context);
		prepareDrawingTools();
		setupGestureDetector();
	}
	public LightsView(Context context, AttributeSet attrs) {
		super(context,attrs);
		prepareDrawingTools();
		setupGestureDetector();
	}
	public LightsView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		prepareDrawingTools();
		setupGestureDetector();
	}
	private void prepareDrawingTools() {
		filledPaint=prepareFilledPaintWithColor(R.color.FilledCell);
		emptyPaint=prepareFilledPaintWithColor(R.color.EmptyCell);
		drawPathF=new Path();
		drawPathE=new Path();
	}
	private void setupGestureDetector() {
		gestureDetector=new GestureDetector(getContext(),new LightsGestureListener());
		scaleGestureDetector=new ScaleGestureDetector(getContext(), new LightsScaleListener());
	}
	private Paint prepareFilledPaintWithColor(int color) {
		Paint p=new Paint();
		p.setColor(getResources().getColor(color));
		p.setAntiAlias(true);
		p.setStyle(Style.FILL);
		return p;
	}
	public void setMatrix(LightsMatrix mat) {
		this.mat=mat;
	}
	public void setPrevAlpha(int alpha) {
		prevAlpha=alpha;
	}
	@Override
	protected void onDraw(Canvas c) {
		super.onDraw(c);
		setPaintAlpha(255);
		drawCells(c,screenCellSize);
		if(prevAlpha>0) {
			drawGhostCells(c,screenCellSize);
		}
	}
	private void drawGhostCells(Canvas c, int size) {
		setPaintAlpha(prevAlpha);
		drawPathF.reset();
		drawPathE.reset();
		int lastX=mat.getLastX();
		int lastY=mat.getLastY();
		int ybottom=Math.min(lastY+2, clipYbottom);
		int xright=Math.min(lastX+2, clipXright);
		for(int y=Math.max(lastY-1, clipYtop); y<ybottom; y++) {
			int ycoord=offsetY+size*y;
			for(int x=Math.max(lastX-1, clipXleft); x<xright; x++) {
				if(y==lastY || x==lastX) {
					int xcoord=offsetX+size*x;
					boolean cellstate=getMatXY(LightsMatrix.MAT_BACK,x,y);
					drawCell(size,xcoord,ycoord,cellstate);
				}
			}
		}
		c.drawPath(drawPathF, filledPaint);
		c.drawPath(drawPathE, emptyPaint);
	}
	private void setPaintAlpha(int alpha) {
		filledPaint.setAlpha(alpha);
		emptyPaint.setAlpha(alpha);
	}
	private void drawCells(Canvas c,int size) {
		drawPathF.reset();
		drawPathE.reset();
		for(int y=clipYtop; y<clipYbottom; y++) {
			int ycoord=offsetY+size*y;
			for(int x=clipXleft; x<clipXright; x++) {
				int xcoord=offsetX+size*x;
				boolean cellstate=getMatXY(LightsMatrix.MAT_FRONT,x,y);
				drawCell(size,xcoord,ycoord,cellstate);
			}
		}
		c.drawPath(drawPathF, filledPaint);
		c.drawPath(drawPathE, emptyPaint);
	}
	private void drawCell(int size, int left, int top, boolean state) {
		Path p=state?drawPathF:drawPathE;
		float padding=.05f*(float)size;
		rect.left=left+padding;
		rect.top=top+padding;
		rect.bottom=top+size-padding;
		rect.right=left+size-padding;
		p.addRoundRect(rect, padding,padding,Path.Direction.CCW);
	}
	private boolean getMatXY(int back, int x, int y) {
		try {
			return mat.getXY(back, x, y);
		} catch (NullPointerException e) {
			return ((x^y)&1)>0;
		}
	}
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		setShaders();
		optimizeViewPort();
	}
	public void optimizeViewPort() {
		clipXleft=0;
		clipXright=getMatWidth();
		clipYtop=0;
		clipYbottom=getMatHeight();
		float viewW=getWidth();
		float viewH=getHeight();
		float fldW=CELLSIZE*clipXright;
		float fldH=CELLSIZE*clipYbottom;
		newZoom(Math.min(viewW/fldW, viewH/fldH));
		initialZoom=zoom;
		float imgX=fldW*zoom;
		float imgY=fldH*zoom;
		offsetX=(int) (viewW-imgX)/2;
		offsetY=(int) (viewH-imgY)/2;
	}
	private void setShaders() {
		Resources r=getResources();
		filledPaint.setShader(new LinearGradient(0.f, 0.f, getWidth(), getHeight(),
				r.getColor(R.color.FilledCell),r.getColor(R.color.FilledCell2) , Shader.TileMode.REPEAT));
		emptyPaint.setShader(new LinearGradient(0.f, 0.f, getWidth(), getHeight(),
				r.getColor(R.color.EmptyCell),r.getColor(R.color.EmptyCell2) , Shader.TileMode.REPEAT));
	}
	private int getMatWidth() {
		try { return mat.getWidth(); } catch(NullPointerException e) { return 8; }
	}
	private int getMatHeight() {
		try { return mat.getHeight(); } catch(NullPointerException e) { return 8; }
	}
	public interface LightsViewEventListener {
		public void cellClicked(LightsView view, int tilex, int tiley);
	}
	private LightsViewEventListener listener;
	public void setLightsViewEventListener(LightsViewEventListener listener) {
		this.listener=listener;
	}
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent evt) {
		boolean scaleP=scaleGestureDetector.onTouchEvent(evt);
		boolean gestureP;
		if(!scaleGestureDetector.isInProgress()) {
			gestureP=gestureDetector.onTouchEvent(evt);
		} else {
			gestureP=false;
		}
		return scaleP || gestureP || super.onTouchEvent(evt);
	}
	private float ScreenToFieldHorizontal(float scrX) {
		return (scrX-((float)(offsetX)))/zoom;
	}
	private float ScreenToFieldVertical(float scrY) {
		return (scrY-((float)(offsetY)))/zoom;
	}
	private class LightsGestureListener extends GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			if(listener!=null) {
				int tileX=(int) Math.floor(ScreenToFieldHorizontal(e.getX())/CELLSIZE);
				int tileY=(int) Math.floor(ScreenToFieldVertical(e.getY())/CELLSIZE);
				if(0<=tileX && tileX<getMatWidth() && 0<=tileY && tileY<getMatHeight()) {
					listener.cellClicked(LightsView.this, tileX, tileY);
					return true;
				}
			}
			return false;
		}
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			float dx=e2.getX()-e1.getX();
			float dy=e2.getY()-e1.getY();
			if(Math.abs(dx)+Math.abs(dy)<screenCellSize) {
				if(e2.getAction()==MotionEvent.ACTION_UP) {
					onSingleTapUp(e2);
				}
				return false;
			}
			offsetXclip((int) (offsetX-distanceX));
			offsetYclip((int) (offsetY-distanceY));
			invalidate();
			return true;
		}
	}
	private class LightsScaleListener implements ScaleGestureDetector.OnScaleGestureListener {
		private float centerx, centery;
		private float screenx, screeny;
		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			screenx=detector.getFocusX();
			screeny=detector.getFocusY();
			centerx=ScreenToFieldHorizontal(screenx);
			centery=ScreenToFieldVertical(screeny);
			return true;
		}
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			float scaleF=detector.getScaleFactor();
			float newzoom=zoom*scaleF;
			newzoom=Math.max(Math.min(ZOOM_MIN,initialZoom), newzoom);
			newzoom=Math.min(Math.max(ZOOM_MAX,initialZoom), newzoom);
			float zoomedFieldX=centerx*newzoom;
			float zoomedFieldY=centery*newzoom;
			newZoom(newzoom);
			offsetXclip((int) (screenx-zoomedFieldX));
			offsetYclip((int) (screeny-zoomedFieldY));
			invalidate();
			return true;
		}
		@Override
		public void onScaleEnd(ScaleGestureDetector detector) { }
	}
	private void newZoom(float newzoom) {
		zoom=newzoom;
		screenCellSize=(int) (zoom*CELLSIZE);
	}
	private void offsetXclip(int newOffsX) {
		int offsX_min=-screenCellSize*(mat.getWidth()-1);
		int offsX_max=getWidth()-screenCellSize;
		offsetX=Math.min(Math.max(offsX_min, newOffsX),offsX_max);
	}
	private void offsetYclip(int newOffsY) {
		int offsY_min=-screenCellSize*(mat.getHeight()-1);
		int offsY_max=getHeight()-screenCellSize;
		offsetY=Math.min(Math.max(offsY_min,newOffsY), offsY_max);
	}
}
