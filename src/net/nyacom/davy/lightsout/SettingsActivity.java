package net.nyacom.davy.lightsout;

import java.text.NumberFormat;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.MenuItem;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	public static final String PREF_WIDTH="lightsWidth";
	public static final String PREF_HEIGHT="lightsHeight";
	public static final String PREF_NLOOPS="lightsLoops";
	// Update res/xml/preferences.xml after changing following values:
	public static final int DEF_WIDTH=5, DEF_HEIGHT=5, DEF_LOOPS=12;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(android.os.Build.VERSION.SDK_INT>=11) {
			addUpToActionbar();
		}
		addPreferencesFromResource(R.xml.preferences);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void addUpToActionbar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences shrP=PreferenceManager.getDefaultSharedPreferences(this);
		shrP.registerOnSharedPreferenceChangeListener(this);
		onSharedPreferenceChanged(shrP,PREF_WIDTH);
		onSharedPreferenceChanged(shrP,PREF_HEIGHT);
		onSharedPreferenceChanged(shrP,PREF_NLOOPS);
	}
	@Override
	public void onPause() {
		SharedPreferences shrP=PreferenceManager.getDefaultSharedPreferences(this);
		shrP.unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int menuId=item.getItemId();
		if(menuId==android.R.id.home) {
			onBackPressed();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		int defValue=-1;
		if(PREF_WIDTH.equals(key)) {
			defValue=DEF_WIDTH;
		} else if(PREF_HEIGHT.equals(key)) {
			defValue=DEF_HEIGHT;
		} else if(PREF_NLOOPS.equals(key)) {
			defValue=DEF_LOOPS;
		}
		if(defValue>=0) {
			Preference p=findPreference(key);
			
			p.setSummary(NumberFormat.getInstance().format(sharedPreferences.getInt(key,defValue)));
		}
	}
}
