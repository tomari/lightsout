package net.nyacom.davy.lightsout;

import java.text.NumberFormat;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class SliderPreference extends DialogPreference {
	private int minValue=0, maxValue=10;
	private NumberFormat numFormat=NumberFormat.getInstance();
	private TextView valueField;
	private SeekBar sb;
	private int currentValue=5;
	private static final String CONF_NAMESPACE="http://davy.nyacom.net/lightsout/schema"; 
	public SliderPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		setDialogLayoutResource(R.layout.layout_sliderpref);
		minValue=attrs.getAttributeIntValue(CONF_NAMESPACE, "minValue", 0);
		maxValue=attrs.getAttributeIntValue(CONF_NAMESPACE, "maxValue", 10);
	}
	@Override
	public void onBindDialogView(View v) {
		super.onBindDialogView(v);
		valueField=(TextView)v.findViewById(R.id.seekBarLabel);
		sb=(SeekBar)v.findViewById(R.id.seekBar);
		sb.setMax(maxValue-minValue);
		sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				valueField.setText(numFormat.format(getCurrentValue()));
			}
		});
		setCurrentValue(getPersistedInt(DEFAULT_VALUE));
	}
	private int getCurrentValue() {
		return sb.getProgress()+minValue;
	}
	private void setCurrentValue(int newValue) {
		currentValue=newValue;
		sb.setProgress(currentValue-minValue);
		valueField.setText(numFormat.format(getCurrentValue()));
	}
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		if(positiveResult) {
			persistInt(getCurrentValue());
		}
	}
	@Override
	protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		super.onSetInitialValue(restorePersistedValue, defaultValue);
		if(restorePersistedValue) {
			currentValue=getPersistedInt(DEFAULT_VALUE);
		} else {
			currentValue=(Integer)defaultValue;
		}
	}
	private static final int DEFAULT_VALUE=3;
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getInteger(index, DEFAULT_VALUE);
	}
	private static class SavedState extends BaseSavedState {
		int value;
		public SavedState(Parcelable superState) { super(superState); }
		public SavedState(Parcel source) {
			super(source);
			value=source.readInt();
		}
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(value);
		}
		// I dont understand why it's here. Just following the Google Documentation
		@SuppressWarnings("unused")
		public static final Parcelable.Creator<SavedState> CREATOR =
				new Parcelable.Creator<SliderPreference.SavedState>() {
					public SavedState createFromParcel(Parcel in) {
						return new SavedState(in);
					}
					public SavedState[] newArray(int size) {
						return new SavedState[size];
					}
				};
	}
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState=super.onSaveInstanceState();
		if(isPersistent()) { return superState; }
		final SavedState myState=new SavedState(superState);
		myState.value=getCurrentValue();
		return myState;
	}
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if(state==null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
		} else {
			SavedState myState=(SavedState) state;
			super.onRestoreInstanceState(myState.getSuperState());
			setCurrentValue(myState.value);
		}
	}
}
